#include<stdio.h>
float attend(int t)	
{
	return (120-(t-15)/5*20);
}
float income(int t)	
{
	return (t*attend(t));
}

float profit(int t)	
{
	return (income(t)-(500+3*attend(t)));
}

int main(){
	int tck, check;
	printf("Enter 1 to find out the Profit for a specific Ticket Price \n and any other integer to find the Ticket price for Maximum Profit:");
	scanf("%d", &check);
	
	if (check==1){
		printf("Enter Ticket Price:");
		scanf("%d", &tck);
		
		printf("Income: %.2f\n",income(tck));
		printf("Profit: %.2f\n",profit(tck));
	}
	else {
		for(tck=10; tck<=30; tck+=5){
			printf("Ticket value: %d ",tck);
			printf("Profit: %.2f\n",profit(tck));
		}
		printf("Therefore a Maximum profit of Rs.1260 can be obtain for the ticket price of Rs.25");
	}
	return 0;
}